/* 

Component : Footer

*/
/** ****************************** Import Packages *************************** */
import React, { Component } from "react";

class Footer extends Component {
    render() {
        return (
            <React.Fragment>
               <div className='footer'>
               <div className="copyright">
                    <p>Copyright ©  pushpros.com</p>
                </div>
               </div>
            </React.Fragment>
        );
    }
}

export default Footer;