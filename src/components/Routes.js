/* 

Component : App Routes

*/
/** ****************************** Import Packages *************************** */
import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

/** ****************************** Import Components And Pages *************** */
import Header from "./Header";
import Footer from "./Footer";
import LandingPage from "./LandingPage";


class Routes extends Component {
	render() {
		return (
			<div className="App">
				<Header />
                <Switch>
                    <Route exact path='/' component={LandingPage} />
                </Switch>
				<Footer />
			</div>
		);
	}
}

export default Routes;