import React, { Component } from "react";
import { Table, Container, Button } from 'react-bootstrap';
import axios from "axios";
class LandingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }
    handleClick = (e) => {
        axios.get("https://aimtell.com/files/sites.json").then(response => {
            if (response.data.count > 0) {
                this.setState({ data: response.data.sites });
            }
        });
    }
    render() {
        return (
            <React.Fragment>
                <Container className='container'>
                    <h1 className='text-center m-5'>Sample Table</h1>
                    <Button className='float-right btn-primary m-3' onClick={this.handleClick}>Load Data</Button>
                    <Table>
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>name</th>
                                <th>url</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.data.length > 0 ?
                                    this.state.data.map((sites, index) => {
                                        return (
                                            <React.Fragment key={index}>
                                                <tr>
                                                    <td>{sites.id}</td>
                                                    <td>{sites.name}</td>
                                                    <td>{sites.url}</td>
                                                </tr>
                                            </React.Fragment>
                                        )
                                    })
                                    : <tr><td></td></tr>

                            }
                        </tbody>
                    </Table>
                </Container>
            </React.Fragment>
        );
    }
}

export default LandingPage;
